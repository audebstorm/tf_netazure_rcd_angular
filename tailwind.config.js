/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{html,ts}",
  ],
  theme: {
    extend: {
      colors : {
        purple : {
          '50' : '#697A21',
          
        }

      }
    },
  },
  plugins: [],
}

