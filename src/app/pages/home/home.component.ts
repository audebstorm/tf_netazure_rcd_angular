import { Component, OnInit } from '@angular/core';
import { User } from '../../shared/models/User';
import { FakeUserService } from '../../shared/services/fake-user.service';
import { CommonModule, DatePipe, JsonPipe, NgClass, UpperCasePipe } from '@angular/common';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [ CommonModule ],
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss'
})
export class HomeComponent implements OnInit {

  users : User[] = [];

  constructor(private fakeUserService : FakeUserService) {

  }

  ngOnInit() : void {
    this.users = this.fakeUserService.getAll();
  }
}
