import { Injectable } from '@angular/core';
import { User } from '../models/User';

@Injectable({
  providedIn: 'root'
})
export class FakeUserService {

  private _users : User[] = [
    { id : 1, firstname : 'Gavin', lastname : 'Chaineux', birthdate : new Date(1997, 9, 18), gender : 'm', avatar : './assets/gavin.png'},
    { id : 2, firstname : 'Aude', lastname : 'Beurive', birthdate : new Date(1989, 9, 16), gender : 'f', avatar : './assets/aude.png'},
    { id : 3, firstname : 'Khun', lastname : 'Ly', birthdate : new Date(1982, 4, 6), gender : 'm', avatar : './assets/khun.png'}
  ]

  constructor() { }

  getAll() : User[] {
    return this._users;
  }
}
