import { Component } from '@angular/core';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-navbar',
  standalone: true,
  imports: [RouterLink],
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.scss'
})
export class NavbarComponent {


  /**
   * Fonction qui permet de dire bonjour en prenant en compte la langue fournie en paramètre
   * @param [string] langage
   */
  direBonjour(langage : string) : void {

  }
}
