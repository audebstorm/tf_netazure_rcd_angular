export interface User {
    id : number;
    lastname : string;
    firstname : string;
    birthdate : Date;
    gender : string;
    avatar : string;
}