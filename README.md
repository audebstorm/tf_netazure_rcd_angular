


<hr>

# Installer Tailwind dans un projet Angular :
## Installer les librairies utiles dans le projet :
```
npm install -D tailwindcss postcss autoprefixer
```
## Créer le fichier de config
```
npx tailwindcss init
```
## Rajouter dans le fichier tailwind.config :
```
module.exports = {
  content: [
    "./src/**/*.{html,ts}",
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}
```
## Rajouter dans le fichier de style global :
```
@tailwind base;
@tailwind components;
@tailwind utilities;
```